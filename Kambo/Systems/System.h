#pragma once
#include "Engine.h"
#include "Typedefs.h"

namespace kambo {

	class Engine;

	class System
	{
	protected:
		const Engine* engine;

	public:
		System(const Engine* engine) { this->engine = engine; }
		virtual ~System() { engine = nullptr; }

		// Perform general start up initialization
		virtual void Init() = 0;

		// Perform initialization related to 3D Graphics
		virtual void InitD3D() = 0;

		// Update the state of the System here... called with a fixed time step dt
		virtual void Update(double t, double dt) = 0;

		// Render the current state of the System... called as often as possible
		virtual void Render(double t, double dt) = 0;

		// Place to destroy and recreate any graphics resources that need to be resized when the window resizes
		virtual void Resize(uint width, uint height) = 0;
	};
}