#pragma once

namespace kambo
{
	typedef long long			int64;
	typedef unsigned long long	uint64;

	typedef signed int			int32;
	typedef unsigned int		uint32;
	typedef unsigned int		uint;

	typedef signed short		int16;
	typedef unsigned short		uint16;
	typedef unsigned short		ushort;

	typedef signed char			int8;
	typedef unsigned char		uint8;
	typedef unsigned char		byte;

	typedef wchar_t				wchar;
}
