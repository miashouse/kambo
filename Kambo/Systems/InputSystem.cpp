#include "pch.h"
#include "InputSystem.h"

namespace kambo {

	InputSystem::InputSystem(const Engine* engine)
		: System(engine)
	{
		
	}

	InputSystem::~InputSystem()
	{
		
	}

	void InputSystem::Init()
	{

	}

	void InputSystem::InitD3D()
	{
		
	}

	void InputSystem::Update(double t, double dt)
	{

	}

	void InputSystem::Render(double t, double dt)
	{
		
	}

	void InputSystem::Resize(uint width, uint height)
	{
		
	}

	// Fills in x and y with the x and y coordinate of the mouse pointer within the window
	void InputSystem::GetMouseXY(int& x, int& y)
	{
		
	}

	// Fills in dx, dy, dz with the deltas from the mouse wheel
	void InputSystem::GetMouseWheel(int& dx, int& dy, int& dz)
	{
		
	}

	bool InputSystem::IsDown(MouseButtons button)
	{
		return false;
	}

	bool InputSystem::IsDown(Keys key)
	{
		return false;
	}

	bool InputSystem::IsUp(MouseButtons button)
	{
		return false;
	}

	bool InputSystem::IsUp(Keys key)
	{
		return false;
	}

	bool InputSystem::WasPressed(MouseButtons button)
	{
		return false;
	}

	bool InputSystem::WasPressed(Keys key)
	{
		return false;
	}

}
